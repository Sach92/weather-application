//Variable Declarations
var APPID = "757bff4c2dc8ceab37f901caa818f5b6"; //Statement Description: Open Weather API Key (nyashasachikonye)
var temp;
var temp2;
var loc;
var loc2;
var icon;
var icon2;
var humidity;
var humidity2;
var wind;
var wind2;
var otherlat;
var otherlat2;
var otherlong;
var otherlong2;
var toggle = true;

var geocomplete_lat;
var geocomplete_lng;

var inputLat;
var inputLong;

var test;
var inputLoc;

function update(weather) {
    icon.src = "imgs/codes/" + weather.code + ".png"
    humidity.innerHTML = "Humidity: " + weather.humidity;
    loc.innerHTML = weather.location;
    temp.innerHTML = weather.temp;
    toggle = false;
}

function update2(weather) {
    icon2.src = "imgs/codes/" + weather.code + ".png"
    humidity2.innerHTML = "Humidity: " + weather.humidity;
    loc2.innerHTML = weather.location;
    temp2.innerHTML = weather.temp;
    toggle = true;
}

function myFunction() {
  test = document.getElementById("location_searchbar");
  inputLoc = test.value;
  sendLRequest(inputLoc);
  // inputLat = document.getElementById("setting_latitude");
  // inputLong = document.getElementById("setting_longitude");
  // geocomplete_lat = inputLat.value;
  // geocomplete_lng = inputLong.value;
  // updateByGeo2(geocomplete_lat,geocomplete_lng);
}

window.onload = function () {
    temp2 = document.getElementById("temperature2");
    temp = document.getElementById("temperature");
    loc2 = document.getElementById("location2");
    loc = document.getElementById("location");
    icon2 = document.getElementById("icon2");
    icon = document.getElementById("icon");
    humidity2 = document.getElementById("humidity2");
    humidity = document.getElementById("humidity");
    inputLat = document.getElementById("setting_latitude");
    inputLong = document.getElementById("setting_longitude");

    if(navigator.geolocation){
	var showPosition = function(position){

	    updateByGeo(position.coords.latitude, position.coords.longitude);
	}
	navigator.geolocation.getCurrentPosition(showPosition);
    } else {
      alert("Please ensure location services are enabled on your browser.")
    }

    // var requestLoc = window.prompt("Which Location Would You Like To Compare Your Weather To?");
    // sendLRequest(requestLoc);

}

function sendRequest(url){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
	    var weather = {};
	    weather.code = data.weather[0].id;
	    weather.humidity = data.main.humidity;
	    weather.wind = data.wind.speed;
	    weather.direction = degreesToDirection(data.wind.deg)
	    weather.location = data.name;
	    weather.temp = K2C(data.main.temp);
	    update(weather);}
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}
function sendRequest2(url){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var data = JSON.parse(xmlhttp.responseText);
	    var weather = {};
	    weather.code = data.weather[0].id;
	    weather.humidity = data.main.humidity;
	    weather.wind = data.wind.speed;
	    weather.direction = degreesToDirection(data.wind.deg)
	    weather.location = data.name;
	    weather.temp = K2C(data.main.temp);
      update2(weather);}
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function sendLRequest(otherlocation){
            var geocoder =  new google.maps.Geocoder();
    geocoder.geocode( { 'address': otherlocation}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            otherlat = results[0].geometry.location.lat();
            otherlong = results[0].geometry.location.lng();
          } else {
            alert("Something Went Wrong " + status);
          }
          updateByGeo2(otherlat,otherlong);
        });
}

/*Update Functions*/
function updateByGeo(lat, lon){
    var url = "http://api.openweathermap.org/data/2.5/weather?" +
	"lat=" + lat +
	"&lon=" + lon +
	"&APPID=" + APPID;
    sendRequest(url);
}
/*Update Functions*/
function updateByGeo2(lat, lon){
    var url = "http://api.openweathermap.org/data/2.5/weather?" +
	"lat=" + lat +
	"&lon=" + lon +
	"&APPID=" + APPID;
    sendRequest2(url);
}

function updateByZip(zip){
    var url = "http://api.openweathermap.org/data/2.5/weather?" +
	"zip=" + zip +
	"&APPID=" + APPID;
    sendRequest(url);
}

/*Helper Functions*/
function degreesToDirection(degrees){
    var range = 360/16;
    var low = 360 - range/2;
    var high = (low + range) % 360;
    var angles = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"];
    for( i in angles ) {
	if(degrees >= low && degrees < high){
	    console.log(angles[i]);
	    return angles[i];
	    console.log("derp");
	}
	low = (low + range) % 360;
	high = (high + range) % 360;
    }
    return "N";

}
function K2F(k){
    return Math.round(k*(9/5)-459.67);
}
function K2C(k){
    return Math.round(k - 273.15);
}
